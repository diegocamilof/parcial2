<?php
require_once "persistencia/Conexion.php";
require_once "persistencia/EstudianteDAO.php";
class Estudiante{
    private $idEstudiante;
    private $nombre;
    private $apellido;
    private $conexion;
    private $estudianteDAO;

    public function getIdEstudiante(){
        return $this -> idEstudiante;
    }

    public function getNombre(){
        return $this -> nombre;
    }

    public function getApellido(){
        return $this -> apellido;
    }

   


    public function Estudiante($idEstudiante = "", $nombre = "", $apellido = ""){
        $this -> idEstudiante = $idEstudiante;
        $this -> nombre = $nombre;
        $this -> apellido = $apellido;
        $this -> conexion = new Conexion();
        $this -> estudianteDAO = new EstudianteDAO($this -> idEstudiante, $this -> nombre, $this -> apellido);
    }

    public function autenticar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> estudianteDAO -> autenticar());
        $this -> conexion -> cerrar();       
        if ($this -> conexion -> numFilas() == 1){            
            $resultado = $this -> conexion -> extraer();
            $this -> idEstudiante = $resultado[0];             
            return true;        
        }else {
            return false;
        }
    }
    
    public function consultar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> estudianteDAO -> consultar());
        $this -> conexion -> cerrar();
        $resultado = $this -> conexion -> extraer();
        $this -> nombre = $resultado[0];
        $this -> apellido = $resultado[1];
      
    }
    public function insertar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> estudianteDAO -> insertar());
        $this -> conexion -> cerrar();
    }
    public function consultarTodos(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> estudianteDAO -> consultarTodos());
        $clientes = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $c = new Estudiante($resultado[0], $resultado[1], $resultado[2]);
            array_push($clientes, $c);
        }
        $this -> conexion -> cerrar();
        return $clientes;
    }  
    
}

?>