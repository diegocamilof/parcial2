<?php
require_once "persistencia/Conexion.php";
require_once "persistencia/CursoDAO.php";
class Estudiante_Curso{
    private $estudiante;
    private $curso;
    private $nota;
    private $conexion;
    private $estudianteCursoDAO;
    
    public function getIdEstudiante(){
        return $this -> estudiante;
    }
    
    public function getNombre(){
        return $this -> curso;
    }
    
    public function getCreditos(){
        return $this -> nota;
    }
    
    
    
    
    public function Estudiante_Curso($estudiante = "", $curso = "", $nota = ""){
        $this -> estudiante = $estudiante;
        $this -> curso = $curso;
        $this -> nota = $nota;
        $this -> conexion = new Conexion();
        $this -> estudianteCursoDAO = new Estudiante_CursoDAO($this -> estudiante, $this -> curso, $this -> nota);
    }
    
    public function autenticar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> estudianteCursoDAO -> autenticar());
        $this -> conexion -> cerrar();
        if ($this -> conexion -> numFilas() == 1){
            $resultado = $this -> conexion -> extraer();
            $this -> estudiante = $resultado[0];
            return true;
        }else {
            return false;
        }
    }
    
    public function consultar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> estudianteCursoDAO -> consultar());
        $this -> conexion -> cerrar();
        $resultado = $this -> conexion -> extraer();
        $this -> curso = $resultado[0];
        $this -> nota = $resultado[1];
        
    }
    public function consultarTodos(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> estudianteCursoDAO -> consultarTodos());
        $clientes = array();
        while(($resultado = $this -> conexion -> extraer()) != null){
            $c = new Curso($resultado[0], $resultado[1], $resultado[2]);
            array_push($clientes, $c);
        }
        $this -> conexion -> cerrar();
        return $clientes;
    }  
    public function insertar(){
        $this -> conexion -> abrir();
        $this -> conexion -> ejecutar($this -> estudianteCursoDAO -> insertar());
        $this -> conexion -> cerrar();
    }
    
}

?>