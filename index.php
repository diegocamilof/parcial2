<?php 
session_start();
require_once "logica/Curso.php";
require_once "logica/Estudiante.php";
require_once "logica/Estudiante_Curso.php";
// require_once "logica/Cliente.php";
// require_once 'logica/Log.php';
// require_once 'logica/Repartidor.php';
// require_once 'logica/Carrito.php';
// require_once 'logica/ProductoCarrito.php';


?>
<html>
<html>
<head>
	<link rel="icon" type="image/png" href="img/logo.png" />
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" >
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.1/css/all.css" />
	<script src="https://code.jquery.com/jquery-3.4.	1.min.js" ></script>
	<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" ></script>	
	<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" ></script>
	<script>
	$(function () {
		  $('[data-toggle="tooltip"]').tooltip()
	})
	</script>
</head>
<body>
<?php 
if (isset($_GET["pid"])) {
    include 'presentacion/menuAdministrador.php';
    $pid = base64_decode($_GET["pid"]);
    include $pid;
} else {
    include 'presentacion/menuAdministrador.php';
    include 'inicio.php';
}
	    
	?>

</body>
</html>