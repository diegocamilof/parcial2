<?php
$c = new Curso();
$cursos = $c -> consultarTodos();
?>
<div class="container mt-3">
	<div class="row">
		<div class="col">
            <div class="card">
				<div class="card-header text-white bg-info">
					<h4>Consultar Producto</h4>
				</div>
				<div class="text-right"><?php echo count($cursos) ?> registros encontrados</div>
              	<div class="card-body">
					<table class="table table-hover table-striped">
						<tr>
							<th>#</th>
							<th>Nombre</th>
							<th>creditos</th>
							<th></th>
						</tr>
						<?php 
						$i=1;
						foreach($cursos as $c){
						    echo "<tr>";						 
						    echo "<td>" . $i . "</td>";
						    echo "<td>" . $c -> getNombre() . "</td>";
						    echo "<td>" . $c -> getCreditos() . "</td>";
						    echo "<td><a href='index.php?pid=". base64_encode("presentacion/consultarEstudianteCurso.php") . "&idCurso=" . $c -> getIdCurso(). "' data-toggle='tooltip' data-placement='left' title='Consultar Estudiantes'><span class='fas fa-search'></span></a></td>";
						    echo "</tr>";
						    $i++;
						}
						?>
					</table>
				</div>
            </div>
		</div>
	</div>
</div>