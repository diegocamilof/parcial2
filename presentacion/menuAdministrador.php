
<nav class="navbar navbar-expand-md navbar-light bg-info">
	<a class="navbar-brand"
		href="index.php?pid=<?php echo base64_encode("inicio.php") ?>"><i
		class="fas fa-home"></i></a>
	<button class="navbar-toggler" type="button" data-toggle="collapse"
		data-target="#navbarSupportedContent"
		aria-controls="navbarSupportedContent" aria-expanded="false"
		aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
	</button>
	<div class="collapse navbar-collapse" id="navbarSupportedContent">
		<ul class="navbar-nav mr-auto">
			<li class="nav-item dropdown active"><a
				class="nav-link dropdown-toggle" href="#" id="navbarDropdown"
				role="button" data-toggle="dropdown" aria-haspopup="true"
				aria-expanded="false">Estudiante</a>
				<div class="dropdown-menu" aria-labelledby="navbarDropdown">
					<a class="dropdown-item"
						href="index.php?pid=<?php echo base64_encode("presentacion/insertarEstudiante.php") ?>">Insertar</a>
					<a class="dropdown-item"
						href="index.php?pid=<?php echo base64_encode("presentacion/consultarEstudiantes.php") ?>">Consultar</a>
				</div></li>
			<li class="nav-item dropdown active"><a
				class="nav-link dropdown-toggle" href="#" id="navbarDropdown"
				role="button" data-toggle="dropdown" aria-haspopup="true"
				aria-expanded="false">Curso</a>
				<div class="dropdown-menu" aria-labelledby="navbarDropdown">
					<a class="dropdown-item"
						href="index.php?pid=<?php echo base64_encode("presentacion/insertarCursos.php") ?>">Insertar</a>
					<a class="dropdown-item"
						href="index.php?pid=<?php echo base64_encode("presentacion/consutarCursos.php") ?>">Consultar</a>
				</div></li>
			<li class="nav-item dropdown active"><a
				class="nav-link dropdown-toggle" href="#" id="navbarDropdown"
				role="button" data-toggle="dropdown" aria-haspopup="true"
				aria-expanded="false">Reportes</a>
				<div class="dropdown-menu" aria-labelledby="navbarDropdown">
					<a class="dropdown-item"
						href="reporteClientesPDF.php" target="_blank">Generar Reporte</a>
						
				</div></li>
		</ul>
		
	</div>
</nav>