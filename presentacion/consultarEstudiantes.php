<?php
$estudiante = new Estudiante();
$estudiantes = $estudiante -> consultarTodos();
?>
<div class="container mt-3">
	<div class="row">
		<div class="col">
            <div class="card">
				<div class="card-header text-white bg-info">
					<h4>Consultar Estudiantes</h4>
				</div>
				<div class="text-right"><?php echo count($estudiantes) ?> registros encontrados</div>
              	<div class="card-body">
					<table class="table table-hover table-striped">
						<tr>
							<th>#</th>
							<th>Nombre</th>
							<th>Apellido</th>
							<th></th>
						</tr>
						<?php 
						$i=1;
						foreach($estudiantes as $e){
						    echo "<tr>";						 
						    echo "<td>" . $i . "</td>";
						    echo "<td>" . $e -> getNombre() . "</td>";
						    echo "<td>" . $e -> getApellido() . "</td>";
						    echo "<td><a href='index.php?pid=". base64_encode("presentacion/AsignarEstudiante.php") . "&idEstudiante=" . $e -> getIdEstudiante(). "' data-toggle='tooltip' data-placement='left' title='Asignar estudiante a un curso'><span class='fas fa-search'></span></a></td>";
						    echo "</tr>";
						    $i++;
						}
						?>
					</table>
				</div>
            </div>
		</div>
	</div>
</div>