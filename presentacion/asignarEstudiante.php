<?php
echo $idEstudiante= $_GET["idEstudiante"];
$idEstudiante= $_GET["idEstudiante"];
$curso= new Curso();
$cursos = $curso -> consultarTodos();
if(isset($_POST["crear"])){
    echo $_POST["curso"];
    $estCur= new Estudiante_Curso($idEstudiante,$_POST["curso"]);
    $estCur -> insertar();
}

?>
<div class="container mt-3">
	<div class="row">
		<div class="col-lg-3 col-md-0"></div>
		<div class="col-lg-6 col-md-12">
            <div class="card">
				<div class="card-header text-white bg-info">
					<h4>Asignar Estudiante</h4>
				</div>
              	<div class="card-body">
					<?php if(isset($_POST["crear"])){ ?>
					<div class="alert alert-success alert-dismissible fade show" role="alert">
						Datos ingresados
						<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
					</div>
					<?php } ?>
					<form action="index.php?pid=<?php echo base64_encode("presentacion/asignarEstudiante.php") ?>" method="post">
						<div class="form-group">
							<label>Curso</label> 
						<select class="form-control" id="curso">
						<?php 
						foreach($cursos as $c){
						    echo '<option selected value="'. $c -> getIdCurso() .'">' . $c -> getNombre() .'</option>';
						}
						?>
                         
                        </select>
							
						</div>
						
						
					

                        
                        
                        <button type="submit" name="crear" class="btn btn-info">Insertar</button>
					</form>
            	</div>
            </div>
		</div>
	</div>
</div>